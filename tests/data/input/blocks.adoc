= Blocks

== Source

[source,ruby]
----
require 'prawn'

Prawn::Document.generate 'example.pdf' do
  text 'Hello, World!'
end
----

== Quote

[verse, Anais Nin, Risk]
____
And then the day came,
when the risk
to remain tight
in a bud
was more painful
than the risk
it took
to blossom.
____
