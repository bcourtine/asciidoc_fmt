mod common;

use asciidoc_lib::io;
use asciidoc_lib::formatters::anchor;
use common::compare_asciidoc_lines;

#[test]
pub fn anchors_should_be_replaced() {
    let asciidoc_lines = io::parse_asciidoc_file("tests/data/input/anchor.adoc").unwrap();

    let corrected = anchor::format(&asciidoc_lines);

    compare_asciidoc_lines("tests/data/expected/anchor_corrected.adoc", &corrected);
}
