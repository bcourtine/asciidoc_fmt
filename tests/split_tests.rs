mod common;

use asciidoc_lib::io;
use asciidoc_lib::formatters::split;
use common::compare_asciidoc_lines;

#[test]
pub fn lorem_ipsum_should_be_split() {
    let asciidoc_lines = io::parse_asciidoc_file("tests/data/input/lorem_ipsum.adoc").unwrap();

    let split = split::format(&asciidoc_lines);

    compare_asciidoc_lines("tests/data/expected/lorem_ipsum_split.adoc", &split);
}

#[test]
pub fn footnotes_should_not_be_split() {
    let asciidoc_lines = io::parse_asciidoc_file("tests/data/input/footnote.adoc").unwrap();

    let split = split::format(&asciidoc_lines);

    compare_asciidoc_lines("tests/data/expected/footnote_split.adoc", &split);
}
