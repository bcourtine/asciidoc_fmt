use std::fs::File;
use std::io::{BufReader, Read};
use std::path::Path;
use asciidoc_lib::model::AsciidocLine;
use asciidoc_lib::io;

#[allow(dead_code)] // False warning
pub fn compare_asciidoc_lines<T>(expected_path: T, asciidoc_lines: &Vec<AsciidocLine>)
    where T: AsRef<Path> {

    let expected_lines = io::parse_asciidoc_file(expected_path).unwrap();

    assert_eq!(expected_lines.len(), asciidoc_lines.len());

    for i in 0..expected_lines.len() {
        assert_eq!(expected_lines[i], asciidoc_lines[i])
    }
}

#[allow(dead_code)] // False warning
pub fn compare_files<T, U>(expected_path: T, test_path: U)
    where T: AsRef<Path>, U: AsRef<Path> {

    let mut expected_file = BufReader::new(File::open(expected_path).unwrap());
    let mut test_file = BufReader::new(File::open(test_path).unwrap());
    let mut expected_content = String::new();
    let mut test_content = String::new();

    expected_file.read_to_string(&mut expected_content).unwrap();
    test_file.read_to_string(&mut test_content).unwrap();

    assert_eq!(expected_content, test_content);
}
