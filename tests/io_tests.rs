extern crate tempfile;

mod common;

use asciidoc_lib::io;
use common::compare_files;

const TEST_FILE: &'static str = "tests/data/input/simple.adoc";

#[test]
pub fn write_file_without_modification_should_result_in_same_file() {
    let lines = io::read_file_lines(TEST_FILE).unwrap();

    let tempdir = tempfile::tempdir().unwrap();
    let path = tempdir.path().join("test.adoc");

    io::write_file_lines(&path, &lines).unwrap();
    compare_files(TEST_FILE, &path);

    tempdir.close().unwrap();
}

#[test]
pub fn write_parsed_adoc_lines_without_modification_should_result_in_same_file() {
    let lines = io::parse_asciidoc_file(TEST_FILE).unwrap();

    let tempdir = tempfile::tempdir().unwrap();
    let path = tempdir.path().join("test.adoc");

    io::write_file_asciidoc_lines(&path, &lines).unwrap();
    compare_files(TEST_FILE, &path);

    tempdir.close().unwrap();
}
