mod common;

use asciidoc_lib::io;
use asciidoc_lib::formatters::merge;
use common::compare_asciidoc_lines;

#[test]
pub fn simple_file_should_be_merged() {
    let asciidoc_lines = io::parse_asciidoc_file("tests/data/input/simple.adoc").unwrap();

    let merged = merge::format(&asciidoc_lines);

    compare_asciidoc_lines("tests/data/expected/simple_merged.adoc", &merged);
}

#[test]
pub fn list_should_be_merged() {
    let asciidoc_lines = io::parse_asciidoc_file("tests/data/input/list.adoc").unwrap();

    let merged = merge::format(&asciidoc_lines);

    compare_asciidoc_lines("tests/data/expected/list_merged.adoc", &merged);
}

#[test]
pub fn lines_inside_blocks_should_not_be_merged() {
    let asciidoc_lines = io::parse_asciidoc_file("tests/data/input/blocks.adoc").unwrap();

    let merged = merge::format(&asciidoc_lines);

    compare_asciidoc_lines("tests/data/input/blocks.adoc", &merged);
}

#[test]
pub fn continuation_should_be_merged() {
    let asciidoc_lines = io::parse_asciidoc_file("tests/data/input/continuation.adoc").unwrap();

    let merged = merge::format(&asciidoc_lines);

    compare_asciidoc_lines("tests/data/expected/continuation_merged.adoc", &merged);
}
