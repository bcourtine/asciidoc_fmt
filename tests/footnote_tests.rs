mod common;

use asciidoc_lib::io;
use asciidoc_lib::formatters::footnote;
use common::compare_asciidoc_lines;

#[test]
pub fn footnotes_should_be_inlined() {
    let asciidoc_lines = io::parse_asciidoc_file("tests/data/input/ftn_ftnref.adoc").unwrap();

    let inlined = footnote::format(&asciidoc_lines);

    compare_asciidoc_lines("tests/data/expected/ftn_ftnref_inlined.adoc", &inlined);
}
