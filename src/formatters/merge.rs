use crate::model::AsciidocLine;

pub fn format(asciidoc_lines: &Vec<AsciidocLine>) -> Vec<AsciidocLine> {
    let mut result = Vec::new();

    let mut current = asciidoc_lines[0].clone();

    for next_line in &asciidoc_lines[1..] {

        if is_new_line_required_after(&current) || is_new_line_required_before(next_line) {
            result.push(current);
            current = next_line.clone();
        } else {
            current.content.push(' ');
            current.content.push_str(&next_line.content.trim());
        }
    }

    result.push(current);

    result
}

pub fn is_new_line_required_before(asciidoc_line: &AsciidocLine) -> bool {
    asciidoc_line.in_block
        || asciidoc_line.is_empty()
        || asciidoc_line.is_block_delimiter()
        || asciidoc_line.is_block_title()
        || asciidoc_line.is_horizontal_break()
        || asciidoc_line.is_page_break()
        || asciidoc_line.is_list_item()
        || asciidoc_line.is_section_title()
        || asciidoc_line.is_property()
        || asciidoc_line.is_style()
}

pub fn is_new_line_required_after(asciidoc_line: &AsciidocLine) -> bool {
    asciidoc_line.in_block
        || asciidoc_line.is_empty()
        || asciidoc_line.is_block_delimiter()
        || asciidoc_line.is_block_title()
        || asciidoc_line.is_horizontal_break()
        || asciidoc_line.is_page_break()
        || asciidoc_line.is_section_title()
        || asciidoc_line.is_property()
        || asciidoc_line.is_style()
        || asciidoc_line.is_continuation()
}
