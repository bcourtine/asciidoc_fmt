use crate::model::AsciidocLine;

pub mod merge;
pub mod split;
pub mod space;
pub mod utf8;
pub mod footnote;
pub mod typo;
pub mod anchor;

pub fn is_formattable(asciidoc_line: &AsciidocLine) -> bool {
    !asciidoc_line.in_block
        && !asciidoc_line.is_empty()
        && !asciidoc_line.is_block_delimiter()
        && !asciidoc_line.is_horizontal_break()
        && !asciidoc_line.is_page_break()
        && !asciidoc_line.is_property()
        && !asciidoc_line.is_style()
}
