use std::collections::HashMap;

use regex::{Captures, Regex};

use crate::formatters::is_formattable;
use crate::model::AsciidocLine;
use crate::{split_line, lines_to_asciidoc_lines};

// In documents converted from HTML pages, footnotes can be represented as links.
// This formatter inlines footnotes with Asciidoc "footnoteref" mmacro.

// Footnote URL format may vary from one site to another.
// Example format is used by "Oracle iPlanet Web Server pages".
lazy_static! {
    pub static ref FTN: Regex = Regex::new(r"\[?link:(?:[^\s]*?)#_ftn([0-9]+)\[?\^*\[[0-9]+\]\^*\](\s?[”’.,;:!?»]+)?\s?").unwrap();
    pub static ref FTNREF: Regex = Regex::new(r"^\[?link:(?:[^\s]*?)#_ftnref([0-9]+)\[?\[[0-9]+\]\]\s?(.+)$").unwrap();
}

pub fn format(asciidoc_lines: &Vec<AsciidocLine>) -> Vec<AsciidocLine> {
    let mut result = Vec::new();

    let lines = asciidoc_lines.iter().map(|l| l.content.to_string()).collect::<Vec<String>>();
    let footnotes_map = build_footnotes_map(&lines);

    for line in asciidoc_lines {
        if is_formattable(line) {
            let with_inline_footnotes = format_line(line, &footnotes_map);
            let footnotes_lines = split_line(with_inline_footnotes);
            result.append(&mut lines_to_asciidoc_lines(&footnotes_lines));
        } else {
            result.push(line.clone())
        }
    }

    result
}

pub fn format_line(asciidoc_line: &AsciidocLine, footnotes: &HashMap<String, String>) -> String {

    let with_inline_footnotes = FTN.replace_all(
        &asciidoc_line.content,
        |caps: &Captures| {
            if footnotes.contains_key(caps.get(1).unwrap().as_str()) {
                let mut ftn = footnotes.get(caps.get(1).unwrap().as_str()).unwrap().to_string();
                let punctuation = match caps.get(2) {
                    None => String::from(""),
                    Some(_match) => _match.as_str().replace(" ", "{nbsp}"),
                };
                ftn.push_str(&punctuation);
                ftn.push_str("\n");
                ftn
            } else {
                caps.get(0).unwrap().as_str().to_string()
            }
        }
    );

    // New asciidoc line cannot be in a block since reformatting of block lines is excluded.
    with_inline_footnotes.to_string()
}

fn build_footnotes_map(lines: &Vec<String>) -> HashMap<String, String> {
    let mut result = HashMap::new();

    for line in lines {
        if FTNREF.is_match(line) {
            for cap in FTNREF.captures_iter(&line) {
                let key = &cap[1];
                let note = &cap[2];
                result.insert(
                    key.to_string(),
                    format!("\nfootnote:{}[{}]", key, note)
                );
            }
        }
    }

    result
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn footnote_shoud_be_recognized() {
        assert_eq!(true, FTN.is_match("link:#_ftn7[[7]]"));
        assert_eq!(true, FTN.is_match("[link:#_ftn12[12]]"));
        assert_eq!(true, FTN.is_match("link:#_ftn33[^^[33]^^]"));
    }

    #[test]
    pub fn footnote_followed_by_punctuation_shoud_be_recognized() {
        assert_eq!(true, FTN.is_match("link:#_ftn7[[10]]."));
        assert_eq!(true, FTN.is_match("[link:#_ftn12[22]]\u{a0}: "));
        assert_eq!(true, FTN.is_match("link:#_ftn33[^^[33]^^] ?"));
    }

    #[test]
    pub fn footnoteref_shoud_be_recognized() {
        assert_eq!(true, FTNREF.is_match("link:#_ftnref7[[7]] A footnote"));
        assert_eq!(true, FTNREF.is_match("[link:#_ftnref44[44]] Another footnote"));
    }
}
