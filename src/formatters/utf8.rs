use regex::Regex;

use crate::model::AsciidocLine;
use crate::formatters::is_formattable;

// Replace UTF-8 characters by asciidoc equivalence sequence.
// cf. https://asciidoctor.org/docs/asciidoc-syntax-quick-reference/#formatted-text
// cf. https://asciidoctor.org/docs/asciidoc-syntax-quick-reference/#text-replacement

lazy_static! {
    pub static ref APOS: Regex = Regex::new(r"(\w)’(\w)").unwrap();
}

pub fn format(asciidoc_lines: &Vec<AsciidocLine>) -> Vec<AsciidocLine> {
    let mut result = Vec::new();

    for line in asciidoc_lines {
        if is_formattable(line) {
            result.push(format_line(line));
        } else {
            result.push(line.clone())
        }
    }

    result
}

pub fn format_line(asciidoc_line: &AsciidocLine) -> AsciidocLine {
    let replace_apos = replace_apos(&asciidoc_line.content);
    let formatted_content = format_content(replace_apos);
    // New asciidoc line cannot be in a block since reformatting of block lines is excluded.
    AsciidocLine::with_context(formatted_content, false)
}

pub fn replace_apos<T>(content: T) -> String where T: AsRef<str> {
    APOS.replace_all(content.as_ref(), "$1'$2").to_string()
}

pub fn format_content<T>(content: T) -> String where T: AsRef<str> {
    content.as_ref()
        .replace("\u{a0}", "{nbsp}")
        // Keep UTF-8 chars for quotes instead of Asciidoc replacements.
        // .replace("“", "\"`")
        // .replace("”", "`\"")
        // .replace("‘", "'`")
        // .replace("’", "`'")
        .replace("©", "(C)")
        .replace("®", "(R)")
        .replace("™", "(TM)")
        .replace("—", "--")
        // Two next replacements are not the same UTF-8 "..." char.
        .replace("…​", "...")
        .replace("…", "...")
        .replace("→", "->")
        .replace("⇒", "=>")
        .replace("←", "<-")
        .replace("⇐", "<=")
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn should_replace_utf8_chars() {
        let without_utf8 = format_content("© Society™");
        assert_eq!("(C) Society(TM)", without_utf8);
    }

    #[test]
    fn should_replace_utf8_arrows() {
        let without_utf8 = format_content("⇒ Word ←");
        assert_eq!("=> Word <-", without_utf8);
    }

    /*
    #[test]
    fn should_replace_utf8_quotes() {
        let without_utf8 = format_content("“Word’");
        assert_eq!("\"`Word`'", without_utf8);
    }
    */

    #[test]
    fn should_replace_utf8_nbsp() {
        let without_nbsp = format_content("«\u{a0}Word\u{a0}»");
        assert_eq!("«{nbsp}Word{nbsp}»", without_nbsp);
    }

    #[test]
    fn should_replace_apos() {
        let without_apos = replace_apos("It’s easy.");
        assert_eq!("It's easy.", without_apos);
    }
}
