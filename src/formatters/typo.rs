use crate::model::AsciidocLine;
use crate::formatters::is_formattable;

// Correction of arbitrary common typo mistakes.

pub fn format(asciidoc_lines: &Vec<AsciidocLine>) -> Vec<AsciidocLine> {
    let mut result = Vec::new();

    for line in asciidoc_lines {
        if is_formattable(line) {
            result.push(format_line(line));
        } else {
            result.push(line.clone())
        }
    }

    result
}

pub fn format_line(asciidoc_line: &AsciidocLine) -> AsciidocLine {
    let formatted_content = format_content(&asciidoc_line.content);
    // New asciidoc line cannot be in a block since reformatting of block lines is excluded.
    AsciidocLine::with_context(formatted_content, false)
}


pub fn format_content<T>(content: T) -> String where T: AsRef<str> {
    content.as_ref()
        .replace("‘‘", "“")
        .replace("’’", "”")
        .replace("__ __", " ")
}


#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn should_replace_quotes() {
        let replace_quotes = format_content("‘‘Word’’");
        assert_eq!("“Word”", replace_quotes);
    }

    #[test]
    fn should_replace_unused_formatters() {
        let without_unused_formatter = format_content("Italic space :__ __.");
        assert_eq!("Italic space : .", without_unused_formatter);
    }
}
