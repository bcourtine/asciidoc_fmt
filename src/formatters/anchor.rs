use regex::Regex;

use crate::formatters::is_formattable;
use crate::model::AsciidocLine;

// Convert document anchors.
lazy_static! {
    pub static ref ANCHOR: Regex = Regex::new(r"\[\[([0-9]+)\.?\]\]").unwrap();
    pub static ref ANCHOR_LINK: Regex = Regex::new(r"link:#([0-9]+)\.?\[(.+?)\]").unwrap();
    pub static ref ARTICLE_NUMBER: Regex = Regex::new(r"^\\([0-9]+)\.\s").unwrap();
}

pub fn format(asciidoc_lines: &Vec<AsciidocLine>) -> Vec<AsciidocLine> {
    let mut result = Vec::new();

    for line in asciidoc_lines {
        if is_formattable(line) {
            result.push(format_line(line));
        } else {
            result.push(line.clone())
        }
    }

    result
}

pub fn format_line(asciidoc_line: &AsciidocLine) -> AsciidocLine {

    let anchor_corrected = ANCHOR.replace_all(&asciidoc_line.content, "[[a$1]]");
    let anchor_link_corrected = ANCHOR_LINK.replace_all(&anchor_corrected, "<<a$1, $2>>");
    let article_number_corrected = ARTICLE_NUMBER.replace_all(&anchor_link_corrected, "*$1.* ");

    // New asciidoc line cannot be in a block since reformatting of block lines is excluded.
    AsciidocLine::with_context(article_number_corrected.to_string(), false)
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn anchor_shoud_be_recognized() {
        assert_eq!(true, ANCHOR.is_match("[[1.]]"));
        assert_eq!(true, ANCHOR.is_match("[[55]]"));
    }

    #[test]
    pub fn anchor_link_shoud_be_recognized() {
        assert_eq!(true, ANCHOR_LINK.is_match("link:#7.[Link to article 7]"));
        assert_eq!(true, ANCHOR_LINK.is_match("link:#22[Link to article 22]"));
    }
}
