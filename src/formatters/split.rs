use regex::Regex;
use crate::model::AsciidocLine;
use crate::{split_line, lines_to_asciidoc_lines};

// 3+ final word length to avoid splits on abbreviations ("cf." for example).
// 10+ length before and after split to avoid short orphan phrase.
// Theses values are arbitrary.
lazy_static! {
    pub static ref SPLIT: Regex = Regex::new(r"(.{6}\w{4}\s?[.?!])\s(.{10})").unwrap();
}

pub fn format(asciidoc_lines: &Vec<AsciidocLine>) -> Vec<AsciidocLine> {
    let mut result = Vec::new();

    for line in asciidoc_lines {
        if is_splittable(line) {
            result.append(&mut split_at_sentences(line));
        } else {
            result.push(line.clone())
        }
    }

    result
}

pub fn is_splittable(asciidoc_line: &AsciidocLine) -> bool {
    !asciidoc_line.in_block
        && !asciidoc_line.is_empty()
        && !asciidoc_line.is_block_delimiter()
        && !asciidoc_line.is_block_title()
        && !asciidoc_line.is_horizontal_break()
        && !asciidoc_line.is_page_break()
        && !asciidoc_line.is_section_title()
        && !asciidoc_line.is_property()
        && !asciidoc_line.is_style()
        && ! asciidoc_line.content.starts_with("footnoteref:")
        && ! asciidoc_line.content.starts_with("footnote:")
}

pub fn split_at_sentences(asciidoc_line: &AsciidocLine) -> Vec<AsciidocLine> {
    let split_content = split_content(&asciidoc_line.content);
    lines_to_asciidoc_lines(&split_content)
}

pub fn split_content<T>(content: T) -> Vec<String> where T: AsRef<str> {
    let split_at_sentences = SPLIT.replace_all(content.as_ref(), "$1\n$2").to_string();
    split_line(split_at_sentences)
}

#[cfg(tests)]
mod tests {

    use super::*;

    #[test]
    pub fn no_split() {
        let split = split_content("A phrase.");
        assert_eq!(1, split.len());
        assert_eq!("A phrase.", split[0]);
    }

    #[test]
    pub fn simple_split() {
        let split = split_content("A phrase. A second phrase.");
        assert_eq!(2, split.len());
        assert_eq!("A phrase.", split[0]);
        assert_eq!("A second phrase.", split[1]);
    }

    #[test]
    pub fn nbsp_exclamation_split() {
        let split = split_content("A phrase\u{a0}! A second phrase.");
        assert_eq!(2, split.len());
        assert_eq!("A phrase\u{a0}!", split[0]);
        assert_eq!("A second phrase.", split[1]);
    }

    #[test]
    pub fn multiple_splits() {
        let split = split_content("A phrase\u{a0}! A good question\u{a0}? A third phrase.");
        assert_eq!(3, split.len());
        assert_eq!("A phrase\u{a0}!", split[0]);
        assert_eq!("A good question\u{a0}?", split[1]);
        assert_eq!("A third phrase.", split[2]);
    }

    #[test]
    pub fn no_split_for_short_abbreviations() {
        let split = split_content("Winter is coming, cf. Games of thrones.");
        assert_eq!(1, split.len());
        assert_eq!("Winter is coming, cf. Games of thrones.", split[0]);
    }

    #[test]
    pub fn no_split_for_short_remaining_orphan() {
        let split = split_content("A phrase. Oh\u{a0}!");
        assert_eq!(1, split.len());
        assert_eq!("A phrase. Oh\u{a0}!", split[0]);
    }
}
