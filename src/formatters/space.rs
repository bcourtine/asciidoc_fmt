use crate::model::AsciidocLine;
use crate::formatters::is_formattable;

// Common space typo corrections.

pub fn format(asciidoc_lines: &Vec<AsciidocLine>) -> Vec<AsciidocLine> {
    let mut result = Vec::new();

    for line in asciidoc_lines {
        if is_formattable(line) {
            result.push(format_line(line));
        } else {
            result.push(line.clone())
        }
    }

    result
}

pub fn format_line(asciidoc_line: &AsciidocLine) -> AsciidocLine {
    let formatted_content = format_content(&asciidoc_line.content);
    // New asciidoc line cannot be in a block since reformatting of block lines is excluded.
    AsciidocLine::with_context(formatted_content, false)
}

pub fn format_content<T>(content: T) -> String where T: AsRef<str> {
    multiple_spaces(french_nbsp(english_quotes(content)))
}

// In french documents, double punctuation requires a "nbsp".
// Only existing spaces are replaced by "nbsp", to avoid unwanted replacements
// (links, Asciidoc macros, etc.).
pub fn french_nbsp<T>(content: T) -> String where T: AsRef<str> {
    content.as_ref()
        .replace("« ", "«\u{a0}")
        .replace(" »", "\u{a0}»")
        .replace(" :", "\u{a0}:")
        .replace(" ;", "\u{a0};")
        .replace(" !", "\u{a0}!")
        .replace(" ?", "\u{a0}?")
        .replace("° ", "°\u{a0}")
}

// English quotes are not separated from content by a space (or nbsp).
pub fn english_quotes<T>(content: T) -> String where T: AsRef<str> {
    content.as_ref()
        .replace("“ ", "“")
        .replace("“\u{a0}", "“")
        .replace("“{nbsp}", "“")
        .replace("‘ ", "‘")
        .replace("‘\u{a0}", "‘")
        .replace("‘{nbsp}", "‘")
        .replace(" ”", "”")
        .replace("\u{a0}”", "”")
        .replace("{nbsp}”", "”")
        .replace(" ’", "’")
        .replace("\u{a0}’", "’")
        .replace("{nbsp}’", "’")
}

// Remove multiple spaces (and nbsp).
pub fn multiple_spaces<T>(content: T) -> String where T: AsRef<str> {
    content.as_ref()
        .replace("\u{a0}\u{a0}", "\u{a0}")
        .replace("{nbsp}{nbsp}", "{nbsp}")
        .replace("\u{a0} ", "\u{a0}")
        .replace("{nbsp} ", "{nbsp}")
        .replace(" \u{a0}", "\u{a0}")
        .replace(" {nbsp}", "{nbsp}")
        .replace("  ", " ")
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn should_add_nbsp() {
        let nbsp = format_content("Here is a quote : « Quote n° 1 ! »");
        assert_eq!("Here is a quote\u{a0}: «\u{a0}Quote n°\u{a0}1\u{a0}!\u{a0}»", nbsp);
    }

    #[test]
    fn should_remove_multiple_spaces() {
        let without_multiple_spaces = format_content("Here {nbsp} is  a phrase  with  multiple  spaces  !");
        assert_eq!("Here{nbsp}is a phrase with multiple spaces\u{a0}!", without_multiple_spaces);
    }

    #[test]
    fn should_remove_spaces_around_english_quotes() {
        let english_text = format_content("“ English quoted text{nbsp}”");
        assert_eq!("“English quoted text”", english_text);
    }
}
