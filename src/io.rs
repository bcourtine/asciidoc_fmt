use std::path::Path;
use std::fs::File;
use std::io::{BufReader, BufRead, BufWriter, Write};

use crate::model::AsciidocLine;
use crate::{lines_to_asciidoc_lines, asciidoc_lines_to_lines};

pub fn parse_asciidoc_file<P: AsRef<Path>>(path: P) -> std::io::Result<Vec<AsciidocLine>> {
    let lines = read_file_lines(path)?;
    let adoc_lines = lines_to_asciidoc_lines(&lines);
    Ok(adoc_lines)
}

pub fn read_file_lines<P: AsRef<Path>>(path: P) -> std::io::Result<Vec<String>> {
    let f = File::open(path)?;
    let reader = BufReader::new(&f);
    let mut result = Vec::new();
    for line in reader.lines() {
        match line {
            Ok(l) => { result.push(l); },
            Err(e) => return Err(e),
        }
    }
    Ok(result)
}

pub fn write_file_asciidoc_lines<P: AsRef<Path>>(path: P, adoc_lines: &Vec<AsciidocLine>) -> std::io::Result<()> {
    let lines = asciidoc_lines_to_lines(adoc_lines);
    write_file_lines(path, &lines)
}

pub fn write_file_lines<P: AsRef<Path>>(path: P, lines: &Vec<String>) -> std::io::Result<()> {
    let file = File::create(path)?;
    let mut file_writer = BufWriter::new(file);

    for line in lines {
        writeln!(file_writer, "{}", line)?;
    }

    Ok(())
}

#[cfg(test)]
mod tests {

    const TEST_FILE: &'static str = "tests/data/input/simple.adoc";

    #[test]
    fn simple_file_should_be_read() {
        let lines = super::read_file_lines(TEST_FILE).unwrap();
        assert_eq!(29, lines.len());
        assert_eq!("= Document Title", lines[0]);
    }

    #[test]
    fn simple_file_should_be_parsed() {
        let adoc_lines = super::parse_asciidoc_file(TEST_FILE).unwrap();
        assert_eq!(29, adoc_lines.len());
        assert_eq!(true, adoc_lines[0].is_section_title());
    }
}
