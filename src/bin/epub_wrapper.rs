#[allow(unused_imports)]
#[macro_use]
extern crate structopt;
extern crate asciidoc_lib;

use std::path::PathBuf;
use structopt::StructOpt;
use asciidoc_lib::model::AsciidocLine;

#[derive(StructOpt, Debug)]
#[structopt(name = "basic")]
struct Opts {
    /// Files to process
    #[structopt(name = "asciidoc file", parse(from_os_str))]
    input: PathBuf,

    /// Front cover image (relative path)
    #[structopt(short = "f", long = "front", parse(from_os_str))]
    front: Option<PathBuf>,

    /// Output file
    #[structopt(short = "o", long = "output", parse(from_os_str))]
    output: Option<PathBuf>,
}

fn main() {
    let opt = Opts::from_args();

    let lines: Vec<AsciidocLine> = asciidoc_lib::io::parse_asciidoc_file(&opt.input).expect("Error reading input asciidoc file");
    let mut result: Vec<AsciidocLine> = lines.into_iter().take_while(|l| !l.is_empty()).collect();

    let doc_title_header = format!(":doctitle: {}", &result[0].content[2..]);
    result.push(AsciidocLine::with_context(doc_title_header, false));

    if opt.front.is_some() {
        let front_path = &opt.front.unwrap();
        let front_path_str = front_path.to_str().expect("Invalid front cover file name");
        let front_header = format!(":front-cover-image: {}", front_path_str);
        result.push(AsciidocLine::with_context(front_header, false));
    }

    result.push(AsciidocLine::with_context("", false));

    let file_name = &opt.input.file_name().expect("Asciidoc path").to_str().expect("Invalid asciidoc file name");
    let include_header = format!("include::{}[]", file_name);
    result.push(AsciidocLine::with_context(include_header, false));

    if opt.output.is_some() {
        asciidoc_lib::io::write_file_asciidoc_lines(opt.output.unwrap(), &result).unwrap();
    } else {
        result.iter().map(|adoc_line| &adoc_line.content).for_each(|l| println!("{}", l));
    }
}
