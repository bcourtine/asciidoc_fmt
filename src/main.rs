#[allow(unused_imports)]
#[macro_use]
extern crate structopt;
extern crate asciidoc_lib;

use std::path::PathBuf;
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name = "basic")]
struct Opts {
    /// Files to process
    #[structopt(name = "asciidoc file", parse(from_os_str))]
    input: PathBuf,

    /// Output file
    #[structopt(short = "o", long = "output", parse(from_os_str))]
    output: Option<PathBuf>,

    /// Remove common french and english typos in space management.
    #[structopt(short = "s", long = "space")]
    space: bool,

    /// Replace UTF-8 chars by Asciidoc equivalents.
    #[structopt(short = "u", long = "utf8")]
    utf8: bool,

    /// Inline footnotes.
    #[structopt(short = "f", long = "footnote")]
    footnote: bool,

    /// Correct internal anchors.
    #[structopt(short = "a", long = "anchor")]
    anchor: bool,

    /// Correct various typo mistakes.
    #[structopt(short = "t", long = "typo")]
    typo: bool,

    /// Apply all formatters
    #[structopt(long = "all")]
    all: bool,
}

fn main() {
    let opt = Opts::from_args();

    let lines = asciidoc_lib::io::parse_asciidoc_file(&opt.input).expect("Error reading input asciidoc file");

    let mut result = asciidoc_lib::formatters::merge::format(&lines);

    /* Custom formatters. */
    if opt.typo || opt.all {
        result = asciidoc_lib::formatters::typo::format(&result);
    }
    if opt.footnote || opt.all {
        result = asciidoc_lib::formatters::footnote::format(&result);
    }
    if opt.anchor || opt.all {
        result = asciidoc_lib::formatters::anchor::format(&result);
    }
    /* End of custom formatters. */

    if opt.space || opt.all {
        result = asciidoc_lib::formatters::space::format(&result);
    }
    if opt.utf8 || opt.all {
        result = asciidoc_lib::formatters::utf8::format(&result);
    }

    result = asciidoc_lib::formatters::split::format(&result);

    if opt.output.is_some() {
        asciidoc_lib::io::write_file_asciidoc_lines(opt.output.unwrap(), &result).unwrap();
    } else {
        result.iter().map(|adoc_line| &adoc_line.content).for_each(|l| println!("{}", l));
    }
}
