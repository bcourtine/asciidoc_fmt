/** Basic Asciidoc line by line parsing. */

use regex::Regex;

pub const HORIZONTAL_BREAK: &'static str = "'''";
pub const PAGE_BREAK: &'static str = "<<<";

lazy_static! {
    pub static ref PROPERTY: Regex = Regex::new(r"^:([^\s]+):(?: (.+))?$").unwrap();
    pub static ref SECTION_TITLE: Regex = Regex::new(r"^(=+) (.+)$").unwrap();
    pub static ref STYLE: Regex = Regex::new(r"^\[([^\[\]]+)\]$").unwrap();
    pub static ref BLOCK_TITLE: Regex = Regex::new(r"^\.(\w.*)$").unwrap();
    // Tables are considered as blocks
    pub static ref BLOCK_DELIMITER: Regex = Regex::new(r"^(-{2,}|_{2,}|={2,}|\.{2,}|\*{2,}|\+{2,}|/{2,}|\|===)$").unwrap();
    pub static ref LIST_ITEM: Regex = Regex::new(r"^(\*+|\.+|[0-9]+\.|-+) (.+)$").unwrap();
}

// By convention, beginning block delimiter is included in block and ending block delimiter is excluded.
#[derive(Debug, Clone, PartialEq)]
pub struct AsciidocLine {
    pub in_block: bool,
    pub content: String,
}

impl AsciidocLine {

    pub fn with_context<T>(content: T, previous_line_in_block: bool) -> Self where T: Into<String> {
        let s = content.into();
        AsciidocLine {
            in_block: BLOCK_DELIMITER.is_match(&s) ^ previous_line_in_block,
            content: s,
        }
    }

    pub fn is_block_delimiter(&self) -> bool {
        BLOCK_DELIMITER.is_match(&self.content)
    }

    pub fn is_property(&self) -> bool {
        PROPERTY.is_match(&self.content)
    }

    pub fn is_section_title(&self) -> bool {
        SECTION_TITLE.is_match(&self.content)
    }

    pub fn is_style(&self) -> bool {
        STYLE.is_match(&self.content)
    }

    pub fn is_block_title(&self) -> bool {
        BLOCK_TITLE.is_match(&self.content)
    }

    pub fn is_list_item(&self) -> bool {
        LIST_ITEM.is_match(&self.content)
    }

    pub fn is_horizontal_break(&self) -> bool {
        &self.content == HORIZONTAL_BREAK
    }

    pub fn is_page_break(&self) -> bool {
        &self.content == PAGE_BREAK
    }

    pub fn is_continuation(&self) -> bool {
        &self.content == "+" || self.content.ends_with(" +")
    }

    pub fn is_empty(&self) -> bool {
        &self.content == ""
    }
}


#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn block_delimiter_should_be_detected() {
        assert_eq!(true, BLOCK_DELIMITER.is_match("=="));
        assert_eq!(true, BLOCK_DELIMITER.is_match("__"));
        assert_eq!(true, BLOCK_DELIMITER.is_match(".."));
        assert_eq!(true, BLOCK_DELIMITER.is_match("++"));
        assert_eq!(true, BLOCK_DELIMITER.is_match("//"));
        assert_eq!(true, BLOCK_DELIMITER.is_match("--"));
        assert_eq!(true, BLOCK_DELIMITER.is_match("**"));
        assert_eq!(true, BLOCK_DELIMITER.is_match("|==="));
    }

    #[test]
    fn block_delimiter_length_is_more_than_1() {
        assert_eq!(false, BLOCK_DELIMITER.is_match("="));
        assert_eq!(false, BLOCK_DELIMITER.is_match("_"));
        assert_eq!(false, BLOCK_DELIMITER.is_match("."));
        assert_eq!(false, BLOCK_DELIMITER.is_match("+"));
        assert_eq!(false, BLOCK_DELIMITER.is_match("/"));
        assert_eq!(false, BLOCK_DELIMITER.is_match("-"));
        assert_eq!(false, BLOCK_DELIMITER.is_match("*"));
    }

    #[test]
    fn title_should_not_be_detected_as_block_delimiter() {
        assert_eq!(false, BLOCK_DELIMITER.is_match("== Title"));
    }

    #[test]
    fn section_title_should_be_detected() {
        assert_eq!(true, SECTION_TITLE.is_match("= Title"));
        assert_eq!(true, SECTION_TITLE.is_match("== Title 2"));
        assert_eq!(true, SECTION_TITLE.is_match("=== Title 3"));
    }

    #[test]
    fn block_title_should_be_detected() {
        assert_eq!(true, BLOCK_TITLE.is_match(".Block title"));
        assert_eq!(false, LIST_ITEM.is_match(".Block title"));
    }

    #[test]
    fn list_item_should_be_detected() {
        assert_eq!(true, LIST_ITEM.is_match(". List item"));
        assert_eq!(true, LIST_ITEM.is_match("** List item"));
        assert_eq!(true, LIST_ITEM.is_match("--- List item"));
        assert_eq!(true, LIST_ITEM.is_match("1. List item"));
        assert_eq!(true, LIST_ITEM.is_match("22. List item"));
    }

    #[test]
    fn style_should_be_detected() {
        assert_eq!(true, STYLE.is_match("[source, language=rust]"));
        assert_eq!(true, STYLE.is_match("[quote, Victor Hugo]"));
        assert_eq!(true, STYLE.is_match("[TIP]"));
        assert_eq!(false, STYLE.is_match("http://link.com[A link]"));
        assert_eq!(false, STYLE.is_match("include::file[]"));
        assert_eq!(false, STYLE.is_match("[[anchor]]"));
    }

    #[test]
    fn property_def_should_be_detected() {
        assert_eq!(true, PROPERTY.is_match(":Author: Document author"));
        assert_eq!(true, PROPERTY.is_match(":Valid_Empty_Prop:"));
        assert_eq!(false, PROPERTY.is_match(":: Empty prop name is invalid"));
        assert_eq!(false, PROPERTY.is_match(":Invalid prop: Prop name must not contain spaces"));
    }

    /** Begin block (table) and end block (quote) does not match. Nested block parsing is not supported yet. */
    #[test]
    fn block_content_should_be_detected() {
        let line_0 = AsciidocLine::with_context("Before block", false);
        let line_1 = AsciidocLine::with_context("|===", line_0.in_block);
        let line_2 = AsciidocLine::with_context("Block content", line_1.in_block);
        let line_3 = AsciidocLine::with_context("Block content 2", line_2.in_block);
        let line_4 = AsciidocLine::with_context("__", line_3.in_block);
        let line_5 = AsciidocLine::with_context("After block", line_4.in_block);

        assert_eq!(false, line_0.in_block);
        assert_eq!(true, line_1.in_block); // Begin block included
        assert_eq!(true, line_2.in_block);
        assert_eq!(true, line_3.in_block);
        assert_eq!(false, line_4.in_block); // End block excluded
        assert_eq!(false, line_5.in_block);
    }
}
