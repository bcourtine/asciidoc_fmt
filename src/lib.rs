#[macro_use]
extern crate lazy_static;
extern crate regex;

use crate::model::AsciidocLine;

pub mod io;
pub mod model;
pub mod formatters;

pub fn lines_to_asciidoc_lines(lines: &Vec<String>) -> Vec<AsciidocLine> {
    let mut result = Vec::new();
    let mut previous_line_in_block = false;

    for line in lines {
        let new = AsciidocLine::with_context(line.to_string(), previous_line_in_block);
        previous_line_in_block = new.in_block;
        result.push(new);
    }

    result
}

pub fn asciidoc_lines_to_lines(adoc_lines: &Vec<AsciidocLine>) -> Vec<String> {
    adoc_lines.iter().map(|adoc_line| adoc_line.content.clone()).collect()
}

pub fn split_line<T>(content: T) -> Vec<String> where T: AsRef<str> {
    let mut result = Vec::new();
    for l in content.as_ref().lines() {
        result.push(l.trim().to_string());
    }
    result
}
